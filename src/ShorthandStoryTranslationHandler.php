<?php

namespace Drupal\shorthand;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for shorthand_story.
 *
 * @deprecated in shorthand:4.0.0 and is removed from shorthand:5.0.0. Use shorthand field.
 *
 * @see https://www.drupal.org/project/shorthand/issues/3274487
 */
class ShorthandStoryTranslationHandler extends ContentTranslationHandler {

}
