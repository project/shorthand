# Shorthand example

Example Shorthand content type with example content.

## Features

* Story content type
* Story field
* Example story downloaded from shorthand
* Creation of the node with the story
